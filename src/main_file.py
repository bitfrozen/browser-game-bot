#!/usr/bin/env python
import sys, logging, re, os
import cookielib
import mechanize
from bs4 import BeautifulSoup

def create_browser_with_page(url):
    browser = mechanize.Browser()
    # setup cookies
    cj = cookielib.LWPCookieJar()
    browser.set_cookiejar(cj)
    # browser options
    browser.set_handle_equiv(True)
    browser.set_handle_gzip(True)
    browser.set_handle_redirect(True)
    browser.set_handle_referer(True)
    browser.set_handle_robots(False)
    # Follows refresh 0 but not hangs on refresh > 0
    browser.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time = 1)
    # Want debugging messages? - No using global debug
    # browser.set_debug_http(True)
    # browser.set_debug_redirects(True)
    # browser.set_debug_responses(True)
    # User-Agent (this is cheating, ok?)
    browser.addheaders = [('User-agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.73.11 (KHTML, like Gecko) Version/6.1.1 Safari/537.73.11')]
    browser.open(url)
    return browser

def login_into_game(browser):
    soup = get_soup(browser)
    # checking it's a correct site
    search = soup.find('input', 'logins')
    if not search.has_attr('value') or search['value'] != 'Vartotojo vardas':
        # throw exeption "Wrong login page"
        print "BAD LOGIN PAGE"
    browser.select_form(nr = 0)
    browser["username"] = "xxx"
    browser["password"] = "xxx"
    # TODO: check form output
    response = browser.submit()
    # wait for page to load
    # check that we successfully logged in
    soup = get_soup(browser)
    search = soup.find('input', 'logins')
    if not search.has_attr('value') or search['value'] != 'Labas, xxx':
        # throw exeption "Wrong login page"
        print "BAD LOGIN PAGE"
    # TODO: follow link into game
    browser.follow_link(text_regex = 'Prisijungti')
    soup = get_soup(browser)
    # check if we are in home page

def enable_logging(level):
    logger = logging.getLogger("mechanize")
    logger.addHandler(logging.StreamHandler(sys.stdout))
    logger.setLevel(level)

def check_alerts(browser):
    browser.open(browser.geturl())  # refresh
    soup = get_soup(browser)
    # safeguard against logout
    if got_logout(soup):
        login_into_game(browser)
        soup = get_soup(browser)
    search = soup.find_all('div', 'alertBox')
    for alert in search:
        # deal with Look for money
        search_alert = alert.find(text = 'Ieškoti pinigų')
        if search_alert:
            # click on link
            browser.follow_link(text_regex = 'Ieškoti pinigų')
        # deal with Look for points
        search_alert = alert.find(text = 'Ieškoti taškų')
        if search_alert:
            # click on link
            browser.follow_link(text_regex = 'Ieškoti taškų')

def write_to_log(text):
    logfile = open('logfile', 'w+')
    print >> logfile, text.encode('utf-8')
    logfile.close()

def got_logout(soup):
    search = soup.find('input', 'logins')
    if search and search.has_attr('value') and search['value'] == 'Vartotojo vardas':
        # we got loged out - log back in
        return True
    return False

def get_status(browser):
    soup = get_soup(browser)
    # safeguard against logout
    if got_logout(soup):
        login_into_game(browser)
        soup = get_soup(browser)
    result = soup.find('div', 'points')
    money = int(re.sub("\D", "", result.div.div.next_sibling.next_sibling.string[:-2]))
    points = int(re.sub("\D", "", result.div.next_sibling.next_sibling.contents[3].string))
    result = soup.find('div', 'progresses')
    current_health = int(result.div.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.div.strong.string.split()[0])
    max_health = int(result.div.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.div.strong.string.split()[2])
    current_energy = int(result.div.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.div.strong.string.split()[0])
    max_energy = int(result.div.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.div.strong.string.split()[2])
    current_vigil = int(result.div.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.div.strong.string.split()[0])
    max_vigil = int(result.div.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.div.strong.string.split()[2])
    current_courage = int(result.div.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.div.strong.string.split()[0])
    max_courage = int(result.div.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.next_sibling.div.strong.string.split()[2])
    return {"money" : money, "points" : points, "current_health" : current_health, "max_health" : max_health, "current_energy" : current_energy, "max_energy" : max_energy, "current_vigil" : current_vigil, "max_vigil" : max_vigil, "current_courage" : current_courage, "max_courage" : max_courage}

def get_soup(browser):
    return BeautifulSoup(browser.response().read(), "lxml")


def open_bank(browser):
    browser.follow_link(text_regex = 'Bankas')
    soup = get_soup(browser)
    # safeguard against logout
    if got_logout(soup):
        login_into_game(browser)
        browser.follow_link(text_regex = 'Bankas')


def put_money_into_bank(browser, amount):
    open_bank(browser)
    browser.select_form(nr = 2)
    browser['damount'] = str(amount)
    response = browser.submit()


def open_crime(browser):
    browser.follow_link(text_regex = 'Nusikaltimai')
    soup = get_soup(browser)
    # safeguard against logout
    if got_logout(soup):
        login_into_game(browser)
        browser.follow_link(text_regex = 'Nusikaltimai')


def perform_crime(browser, crime):
    crime_list = ["3", "55", "4", "5", "57", "58", "6"]
    browser.follow_link(url_regex = 'crime.*' + crime_list[crime - 4])


def refresh(browser):
    browser.open(browser.geturl())
    soup = get_soup(browser)
    # safeguard against logout
    if got_logout(soup):
        login_into_game(browser)


def main():
    # enable logging
    enable_logging(logging.DEBUG)
    # create browser
    mech = create_browser_with_page("http://www.miestukovos.lt/index.php")
    login_into_game(mech)
    # At this point we should be at main section of page
    # Check if there are alerts (supported types 'Look for points' 'Look for money'
    search = soup.find('div', 'alert')
    print mech.response().get_data()
    # get each page throu parser before looking at it
    soup = BeautifulSoup(mech.response().read(), "lxml")

if __name__ == '__main__':
    main()
